﻿using System;

namespace FileSearch.Models
{
	[Serializable]
	public class Parameters: IEquatable<Parameters>
	{
		public string MainFolder { get; set; }

		public string Pattern { get; set; }

		public string SearchText { get; set; }

		public override bool Equals(object obj) =>
			this.Equals(obj as Parameters);

		public bool Equals(Parameters other) =>
			other!= null && 
			String.Equals(MainFolder, other.MainFolder) &&
			String.Equals(Pattern, other.Pattern) &&
			String.Equals(SearchText, other.SearchText);
	}
}
