﻿namespace FileSearch.Models
{
	public class File
	{
		public string FullName { get; set; }

		public string Name { get; set; }

		public string RootFolder { get; set; }
	}
}
