﻿using FileSearch.Abstract;
using FileSearch.Exstensions;
using FileSearch.Models;
using FileSearch.Services;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileSearch
{
	public partial class MainForm : Form, IFileSearchResultObserver
	{

		public MainForm()
		{
			InitializeComponent();
		}

		public void NextFile(string name)
		{
			Thread.Sleep(300);
			this.SetAndIncrementCountSafe();
			this.SetCurrentFileSafe(name);
			
			
		}

		public void RightFile(Models.File file)
		{
			var rootName = file.RootFolder;
			var path = file.FullName.SplitPath();
			var rootPath = rootName.SplitPath();
			var shortPath = path.Skip(rootPath.Count() - 1);
			var nodes = FileTree.Nodes;
			var prevNode = default(TreeNode);

			foreach (var name in shortPath)
			{
				var keyNodes = nodes.Find(name, false);
				var node = default(TreeNode);
				if (keyNodes.Length == 0)
				{
					node = new TreeNode(name)
					{
						Name = name
					};
					this.AddNodeSafe(nodes, node, prevNode);
				}
				else
				{
					node = keyNodes[0];
				}

				nodes = node.Nodes;
				prevNode = node;
			}
		}

		public void ReadingFileError(string name) => this.SetErrorSafe(name);

		public void SearchFinished()
		{
			_finish = true;
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			if (System.IO.File.Exists(parametersFileName))
			{
				using (var fs = new FileStream(parametersFileName, FileMode.OpenOrCreate))
				{
					var parameters = BinarySerializator.Deserialize<Parameters>(fs);
					this.SetParameters(parameters);
				}
			}

			if (String.IsNullOrWhiteSpace(CatalogEditor.Text))
			{
				SearchButton.Enabled = false;
			}

			_timer = new System.Timers.Timer(1000);
			_timer.Elapsed += (s, args) => this.TimerElapsed();
		}

		private void MainForm_Closing(object sender, FormClosingEventArgs e)
		{
			var parameters = new Parameters
			{
				MainFolder = CatalogEditor.Text,
				Pattern = PatternEditor.Text,
				SearchText = SearchEditor.Text
			};

			using (var fs = new FileStream(parametersFileName, FileMode.OpenOrCreate))
			{
				BinarySerializator.Serialize(parameters, fs);
			}
		}

		private void CatalogButton_Click(object sender, EventArgs e)
		{
			this.GetMainFolderName();
		}

		private void GetMainFolderName()
		{
			var dialog = new FolderBrowserDialog();
			var result = dialog.ShowDialog();
			if (result == DialogResult.OK && !String.IsNullOrWhiteSpace(dialog.SelectedPath))
			{
				CatalogEditor.Text = dialog.SelectedPath;
				SearchButton.Enabled = true;
			}
		}

		private async void SearchButton_Click(object sender, EventArgs e)
		{

			PauseButton.Enabled = true;
			SearchButton.Enabled = false;

			var newParams = new Parameters
			{
				MainFolder = CatalogEditor.Text,
				Pattern = PatternEditor.Text,
				SearchText = SearchEditor.Text
			};

			_timer.Start();

			if (!newParams.Equals(_parameters) || _finish)
			{
				_finish = false;
				_source?.Cancel();
				_count = 0;
				_elapsedTime = 0;
				FileTree.Nodes.Clear();
				_parameters = newParams;
				await this.StartSearch();
			}
			else
			{
				await this.ContinueSearch();
			}
			_timer.Stop();

			PauseButton.Enabled = false;
			SearchButton.Enabled = true;
			
		}

		private void PauseButton_Click(object sender, EventArgs e)
		{
			PauseButton.Enabled = false;
			SearchButton.Enabled = true;
			_timer.Stop();
			_source.Cancel();
		}


		private async Task StartSearch()
		{
			var folder = CatalogEditor.Text;
			var pattern = PatternEditor.Text;
			var search = SearchEditor.Text;
			_source = new CancellationTokenSource();
			var token = _source.Token;
			_fileService = new FileService(this, folder, search, pattern);
			await Task.Run(() => _fileService.StartSearch(token));
		}

		private async Task ContinueSearch()
		{
			_source = new CancellationTokenSource();
			var token = _source.Token;
			_timer.Start();
			await Task.Run(() => _fileService.ContinueSearch(token));
		}


		private void ClearAllEditors()
		{
			CatalogEditor.Text = String.Empty;
			PatternEditor.Text = String.Empty;
			SearchEditor.Text = String.Empty;
		}

		private void SetParameters(Parameters p)
		{
			CatalogEditor.Text = p.MainFolder;
			PatternEditor.Text = p.Pattern;
			SearchEditor.Text = p.SearchText;
		}

		private void TimerElapsed()
		{ 
			this.SetAndIncrementTimerSafe();
		}

		private void AddNodeSafe(TreeNodeCollection nodes, TreeNode node, TreeNode prevNode)
		{
			this.Invoke(new Action<TreeNodeCollection, TreeNode, TreeNode>(this.AddNode), new object[] { nodes, node, prevNode });
		}

		private void AddNode(TreeNodeCollection nodes, TreeNode node, TreeNode prevNode)
		{
			nodes.Add(node);
			prevNode?.Expand();
		}

		private void SetCurrentFileSafe(string text)
		{
			this.Invoke(new Action<string>(t => CurrentFile.Text = t), new object[] { text });
		}

		private void SetAndIncrementCountSafe()
		{
			this.Invoke(new Action(() => { ++_count; Count.Text = _count.ToString(); } ), new object[] { });
		}

		private void SetAndIncrementTimerSafe()
		{
			this.Invoke(new Action(() => { ++_elapsedTime; ElapsedTime.Text = _elapsedTime.ToString(); }), new object[] {});
		}

		private void SetErrorSafe(string text)
		{
			this.Invoke(new Action<string>(t => Error.Text = t), new object[] { text });
		}
		

		private int _count;
		private int _elapsedTime;
		private System.Timers.Timer _timer;
		private bool _finish;
		private FileService _fileService;
		private CancellationTokenSource _source;
		private Parameters _parameters = new Parameters();
		private const string parametersFileName = "parameters.txt";
	}
}
