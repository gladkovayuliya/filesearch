﻿using System.Windows.Forms;

namespace FileSearch
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.CatalogButton = new System.Windows.Forms.Button();
			this.CatalogEditor = new System.Windows.Forms.TextBox();
			this.CatalogLabel = new System.Windows.Forms.Label();
			this.PatternLabel = new System.Windows.Forms.Label();
			this.PatternEditor = new System.Windows.Forms.TextBox();
			this.SearchLabel = new System.Windows.Forms.Label();
			this.SearchEditor = new System.Windows.Forms.RichTextBox();
			this.FileTree = new System.Windows.Forms.TreeView();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.CurrentFileInfo = new System.Windows.Forms.ToolStripStatusLabel();
			this.CurrentFile = new System.Windows.Forms.ToolStripStatusLabel();
			this.CountLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.Count = new System.Windows.Forms.ToolStripStatusLabel();
			this.ElapsedTimeLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.ElapsedTime = new System.Windows.Forms.ToolStripStatusLabel();
			this.ErrorLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.Error = new System.Windows.Forms.ToolStripStatusLabel();
			this.SearchButton = new System.Windows.Forms.Button();
			this.PauseButton = new System.Windows.Forms.Button();
			this.statusStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// CatalogButton
			// 
			this.CatalogButton.Location = new System.Drawing.Point(800, 26);
			this.CatalogButton.Name = "CatalogButton";
			this.CatalogButton.Size = new System.Drawing.Size(75, 23);
			this.CatalogButton.TabIndex = 0;
			this.CatalogButton.Text = "Выбрать";
			this.CatalogButton.UseVisualStyleBackColor = true;
			this.CatalogButton.Click += new System.EventHandler(this.CatalogButton_Click);
			// 
			// CatalogEditor
			// 
			this.CatalogEditor.Location = new System.Drawing.Point(479, 28);
			this.CatalogEditor.Name = "CatalogEditor";
			this.CatalogEditor.ReadOnly = true;
			this.CatalogEditor.Size = new System.Drawing.Size(303, 20);
			this.CatalogEditor.TabIndex = 1;
			// 
			// CatalogLabel
			// 
			this.CatalogLabel.AutoSize = true;
			this.CatalogLabel.Location = new System.Drawing.Point(357, 31);
			this.CatalogLabel.Name = "CatalogLabel";
			this.CatalogLabel.Size = new System.Drawing.Size(107, 13);
			this.CatalogLabel.TabIndex = 2;
			this.CatalogLabel.Text = "Начальный каталог\r\n";
			// 
			// PatternLabel
			// 
			this.PatternLabel.AutoSize = true;
			this.PatternLabel.Location = new System.Drawing.Point(357, 73);
			this.PatternLabel.Name = "PatternLabel";
			this.PatternLabel.Size = new System.Drawing.Size(116, 13);
			this.PatternLabel.TabIndex = 3;
			this.PatternLabel.Text = "Шаблон имени файла";
			// 
			// PatternEditor
			// 
			this.PatternEditor.Location = new System.Drawing.Point(479, 66);
			this.PatternEditor.Name = "PatternEditor";
			this.PatternEditor.Size = new System.Drawing.Size(303, 20);
			this.PatternEditor.TabIndex = 4;
			// 
			// SearchLabel
			// 
			this.SearchLabel.AutoSize = true;
			this.SearchLabel.Location = new System.Drawing.Point(357, 183);
			this.SearchLabel.Name = "SearchLabel";
			this.SearchLabel.Size = new System.Drawing.Size(86, 13);
			this.SearchLabel.TabIndex = 5;
			this.SearchLabel.Text = "Искомый текст";
			// 
			// SearchEditor
			// 
			this.SearchEditor.Location = new System.Drawing.Point(479, 120);
			this.SearchEditor.Name = "SearchEditor";
			this.SearchEditor.Size = new System.Drawing.Size(303, 123);
			this.SearchEditor.TabIndex = 8;
			this.SearchEditor.Text = "";
			// 
			// FileTree
			// 
			this.FileTree.Location = new System.Drawing.Point(12, 26);
			this.FileTree.Name = "FileTree";
			this.FileTree.Size = new System.Drawing.Size(311, 492);
			this.FileTree.TabIndex = 9;
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CurrentFileInfo,
            this.CurrentFile,
            this.CountLabel,
            this.Count,
            this.ElapsedTimeLabel,
            this.ElapsedTime,
            this.ErrorLabel,
            this.Error});
			this.statusStrip1.Location = new System.Drawing.Point(0, 544);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(887, 22);
			this.statusStrip1.TabIndex = 10;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// CurrentFileInfo
			// 
			this.CurrentFileInfo.Name = "CurrentFileInfo";
			this.CurrentFileInfo.Size = new System.Drawing.Size(92, 17);
			this.CurrentFileInfo.Text = "Текущий файл:";
			// 
			// CurrentFile
			// 
			this.CurrentFile.Name = "CurrentFile";
			this.CurrentFile.Size = new System.Drawing.Size(0, 17);
			this.CurrentFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// CountLabel
			// 
			this.CountLabel.Name = "CountLabel";
			this.CountLabel.Size = new System.Drawing.Size(111, 17);
			this.CountLabel.Text = "Обработано всего:";
			// 
			// Count
			// 
			this.Count.Name = "Count";
			this.Count.Size = new System.Drawing.Size(0, 17);
			// 
			// ElapsedTimeLabel
			// 
			this.ElapsedTimeLabel.Name = "ElapsedTimeLabel";
			this.ElapsedTimeLabel.Size = new System.Drawing.Size(116, 17);
			this.ElapsedTimeLabel.Text = "Прошедшее время:";
			// 
			// ElapsedTime
			// 
			this.ElapsedTime.Name = "ElapsedTime";
			this.ElapsedTime.Size = new System.Drawing.Size(13, 17);
			this.ElapsedTime.Text = "0";
			// 
			// ErrorLabel
			// 
			this.ErrorLabel.Name = "ErrorLabel";
			this.ErrorLabel.Size = new System.Drawing.Size(135, 17);
			this.ErrorLabel.Text = "Ошибка чтения файла:";
			// 
			// Error
			// 
			this.Error.Name = "Error";
			this.Error.Size = new System.Drawing.Size(0, 17);
			// 
			// SearchButton
			// 
			this.SearchButton.Location = new System.Drawing.Point(448, 289);
			this.SearchButton.Name = "SearchButton";
			this.SearchButton.Size = new System.Drawing.Size(133, 30);
			this.SearchButton.TabIndex = 11;
			this.SearchButton.Text = "Искать";
			this.SearchButton.UseVisualStyleBackColor = true;
			this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
			// 
			// PauseButton
			// 
			this.PauseButton.Enabled = false;
			this.PauseButton.Location = new System.Drawing.Point(649, 289);
			this.PauseButton.Name = "PauseButton";
			this.PauseButton.Size = new System.Drawing.Size(133, 30);
			this.PauseButton.TabIndex = 13;
			this.PauseButton.Text = "Пауза";
			this.PauseButton.UseVisualStyleBackColor = true;
			this.PauseButton.Click += new System.EventHandler(this.PauseButton_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(887, 566);
			this.Controls.Add(this.PauseButton);
			this.Controls.Add(this.SearchButton);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.FileTree);
			this.Controls.Add(this.SearchEditor);
			this.Controls.Add(this.SearchLabel);
			this.Controls.Add(this.PatternEditor);
			this.Controls.Add(this.PatternLabel);
			this.Controls.Add(this.CatalogLabel);
			this.Controls.Add(this.CatalogEditor);
			this.Controls.Add(this.CatalogButton);
			this.Name = "MainForm";
			this.Text = "Поиск файлов";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_Closing);
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button CatalogButton;
		private System.Windows.Forms.TextBox CatalogEditor;
		private System.Windows.Forms.Label CatalogLabel;
		private System.Windows.Forms.Label PatternLabel;
		private System.Windows.Forms.TextBox PatternEditor;
		private System.Windows.Forms.Label SearchLabel;
		private System.Windows.Forms.RichTextBox SearchEditor;
		private System.Windows.Forms.TreeView FileTree;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel CurrentFileInfo;
		private System.Windows.Forms.ToolStripStatusLabel CurrentFile;
		private System.Windows.Forms.ToolStripStatusLabel CountLabel;
		private System.Windows.Forms.ToolStripStatusLabel Count;
		private System.Windows.Forms.ToolStripStatusLabel ElapsedTimeLabel;
		private System.Windows.Forms.ToolStripStatusLabel ElapsedTime;
		private System.Windows.Forms.Button SearchButton;
		private System.Windows.Forms.Button PauseButton;
		private ToolStripStatusLabel ErrorLabel;
		private ToolStripStatusLabel Error;
	}
}

