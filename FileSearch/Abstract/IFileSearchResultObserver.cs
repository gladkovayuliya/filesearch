﻿using FileSearch.Models;

namespace FileSearch.Abstract
{
	public interface IFileSearchResultObserver
	{
		void NextFile(string name);

		void RightFile(File file);

		void ReadingFileError(string name);

		void SearchFinished();
	}
}
