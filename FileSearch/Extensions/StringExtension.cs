﻿using System;

namespace FileSearch.Exstensions
{
	public static class StringExtension
	{
		public static string[] SplitPath(this string path) => path.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);
	}
}
