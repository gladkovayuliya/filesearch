﻿using System.IO;

namespace FileSearch.Exstensions
{
	public static class FileInfoExtension
	{
		public static bool IsSystem(this FileInfo info) => info.Attributes.HasFlag(FileAttributes.System);

		public static bool IsHidden(this FileInfo info) => info.Attributes.HasFlag(FileAttributes.Hidden);

		public static bool IsTemporary(this FileInfo info) => info.Attributes.HasFlag(FileAttributes.Temporary);
	}
}
