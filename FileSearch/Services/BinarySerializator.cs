﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace FileSearch.Services
{
	public static class BinarySerializator
	{
		public static T Deserialize<T>(Stream stream)
		{
			var bf = new BinaryFormatter();
			return (T)bf.Deserialize(stream);
		}

		public static void Serialize<T>(T ob, Stream stream)
		{
			var bf = new BinaryFormatter();
			bf.Serialize(stream, ob);

		}
	}
}
