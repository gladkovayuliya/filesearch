﻿using FileSearch.Abstract;
using FileSearch.Exstensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;

namespace FileSearch.Services
{
	public class FileService
	{
		public FileService(IFileSearchResultObserver observer,
						   string mainFolder,
						   string searchText,
						   string pattern)
		{
			_observer = observer ?? throw new ArgumentNullException(nameof(observer));
			_root = mainFolder ?? throw new ArgumentNullException(nameof(mainFolder));
			_searchText = searchText ?? throw new ArgumentNullException(nameof(searchText));

			if (pattern == null)
			{
				throw new ArgumentNullException(nameof(pattern));
			}
			_regex = this.FileMaskToRegex(pattern);

		}

		public void StartSearch(CancellationToken token)
		{
			this.CheckFolder(_root, token);
			if (!token.IsCancellationRequested)
			{
				_observer.SearchFinished();
			}
		}

		public void ContinueSearch(CancellationToken token)
		{
			
			var path = _fileToContinueWith == null
				? _savedFolder.SplitPath()
				: _fileToContinueWith.SplitPath();
			var rootPath = _root.SplitPath();
			var shortPath = path.Skip(rootPath.Count() - 1).ToArray();
			var startFolder = _fileToContinueWith == null
				?_savedFolder
				: Path.GetDirectoryName(_fileToContinueWith);

			if (_fileToContinueWith != null)
			{
				var files = Directory.GetFiles(startFolder).SkipWhile(name => !String.Equals(name, _fileToContinueWith));
				this.CheckFiles(files, token);
				if (token.IsCancellationRequested)
				{
					return;
				}
			}

			var restDirectories = Directory.GetDirectories(startFolder);
			this.CheckFolders(restDirectories, token);
			if (token.IsCancellationRequested)
			{
				return;
			}

			var indent = _fileToContinueWith == null
				? 2
				: 3;

			for (var i = shortPath.Length - indent; i >= 0; --i)
			{
				var parent = Path.GetDirectoryName(startFolder);
				var folders = Directory.GetDirectories(parent).SkipWhile(name => !String.Equals(name, startFolder)).Skip(1);
				this.CheckFolders(folders, token);
				startFolder = parent;
				if (token.IsCancellationRequested)
				{
					return;
				}
			}

			_observer.SearchFinished();
	
		}

		private void CheckFiles(IEnumerable<string> files, CancellationToken token)
		{
			foreach (var fileName in files)
			{
				if (token.IsCancellationRequested)
				{
					this.SaveState(fileName);
					return;
				}

				_observer.NextFile(Path.GetFileName(fileName));

				if (this.CheckFile(fileName))
				{
					var file = new Models.File
					{
						FullName = fileName,
						Name = Path.GetFileName(fileName),
						RootFolder = _root
					};
					_observer.RightFile(file);
				}
			}
		}

		private void CheckFolder(string folder, CancellationToken token)
		{
			try
			{
				_savedFolder = folder;
				var files = Directory.GetFiles(folder);
				var nestedFolders = Directory.GetDirectories(folder);

				this.CheckFiles(files, token);
				if (token.IsCancellationRequested)
				{
					return;
				}

				this.CheckFolders(nestedFolders, token);

				if (token.IsCancellationRequested)
				{
					return;
				}

			}
			catch (UnauthorizedAccessException)
			{
				return;
			}
		}

		private void CheckFolders(IEnumerable<string> folders, CancellationToken token)
		{
			foreach (var folder in folders)
			{
				this.CheckFolder(folder, token);
				if (token.IsCancellationRequested)
				{
					return;
				}
			}
		}


		private void SaveState(string fileName)
		{
			_fileToContinueWith = fileName;
		}

		

		private bool CheckFile(string path)
		{
			var name = Path.GetFileName(path);
			var fileInfo = new FileInfo(path);

			if (!_regex.IsMatch(name) || fileInfo.IsHidden() || fileInfo.IsSystem() || fileInfo.IsTemporary() || fileInfo.Length > maxFileLength)
			{
				return false;
			}

			try
			{
				using (var reader = new StreamReader(path))
				{
					var text = reader.ReadToEnd();
					if (text.Contains(_searchText))
					{
						return true;
					}
				}
			}
			catch (IOException)
			{
				_observer.ReadingFileError(Path.GetFileName(path));
			}

			return false;
		}

		private Regex FileMaskToRegex(string fileMask)
		{
			String convertedMask = "^" + Regex.Escape(fileMask).Replace("\\*", ".*").Replace("\\?", ".") + "$";
			if (convertedMask.Length == 2)
			{
				convertedMask = String.Empty;
			}
			return new Regex(convertedMask, RegexOptions.IgnoreCase);
		}

		private readonly IFileSearchResultObserver _observer;
		private readonly string _searchText;
		private string _root;
		private readonly Regex _regex;
		private string _fileToContinueWith;
		private string _savedFolder;
		private const int maxFileLength = 80 * 1024 * 1024;
	}
}
